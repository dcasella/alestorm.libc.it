# Global Postfix configuration file. This file lists only a subset
# of all parameters. For the syntax, and for a complete parameter
# list, see the postconf(5) manual page (command: "man 5 postconf").
#
# For common configuration examples, see BASIC_CONFIGURATION_README
# and STANDARD_CONFIGURATION_README. To find these documents, use
# the command "postconf html_directory readme_directory", or go to
# http://www.postfix.org/BASIC_CONFIGURATION_README.html etc.
#
# For best results, change no more than 2-3 parameters at a time,
# and test if Postfix still works after every change.

# COMPATIBILITY
#
# The compatibility_level determines what default settings Postfix
# will use for main.cf and master.cf settings. These defaults will
# change over time.
#
# To avoid breaking things, Postfix will use backwards-compatible
# default settings and log where it uses those old backwards-compatible
# default settings, until the system administrator has determined
# if any backwards-compatible default settings need to be made
# permanent in main.cf or master.cf.
#
# When this review is complete, update the compatibility_level setting
# below as recommended in the RELEASE_NOTES file.
#
# The level below is what should be used with new (not upgrade) installs.
#
compatibility_level = 3

# Custom configuration follows

mydomain = libc.it
myhostname = mx.$mydomain
myorigin = $mydomain
mydestination = $myhostname, localhost.$mydomain, localhost
mynetworks = [::1]/128 127.0.0.1/32 172.17.0.0/16
relayhost =

inet_interfaces = all
inet_protocols = all

virtual_transport = lmtp:unix:private/dovecot-lmtp
virtual_mailbox_domains = proxy:mysql:/etc/postfix/sql/virtual-domain-maps.cf
virtual_mailbox_maps = proxy:mysql:/etc/postfix/sql/virtual-mailbox-maps.cf,
	proxy:mysql:/etc/postfix/sql/virtual-alias-maps.cf,
	proxy:mysql:/etc/postfix/sql/virtual-alias-domain-mailbox-maps.cf
virtual_alias_maps = proxy:mysql:/etc/postfix/sql/virtual-alias-maps.cf,
	proxy:mysql:/etc/postfix/sql/virtual-alias-domain-maps.cf,
	proxy:mysql:/etc/postfix/sql/virtual-alias-domain-catchall-maps.cf

smtp_tls_loglevel = 1
smtp_tls_security_level = may
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache

smtpd_banner = $myhostname ESMTP $mail_name (Debian/GNU)
smtpd_delay_reject = yes
smtpd_helo_required = yes
smtpd_sender_login_maps = $virtual_mailbox_maps
smtpd_client_restrictions = permit_mynetworks,
	permit_sasl_authenticated,
	reject_rbl_client bl.spamcop.net
smtpd_helo_restrictions = permit_mynetworks,
	permit_sasl_authenticated,
	reject_invalid_helo_hostname
smtpd_sender_restrictions =
	reject_non_fqdn_sender,
	reject_unknown_sender_domain,
	permit_mynetworks,
	reject_unlisted_sender,
	reject_sender_login_mismatch,
	permit_sasl_authenticated
smtpd_relay_restrictions = permit_mynetworks,
	permit_sasl_authenticated,
	reject_unauth_destination
smtpd_recipient_restrictions = reject_unauth_destination,
	reject_non_fqdn_recipient,
	reject_unknown_recipient_domain,
	reject_unlisted_recipient,
	reject_unverified_recipient,
	permit_mynetworks,
	permit_sasl_authenticated
smtpd_end_of_data_restrictions =
	check_recipient_access regexp:/etc/postfix/maps/smtpd-eod-regexp,
	permit
smtpd_sasl_auth_enable = yes
smtpd_sasl_authenticated_header = yes
smtpd_sasl_path = private/auth
smtpd_sasl_security_options = noanonymous, noplaintext
smtpd_sasl_tls_security_options = noanonymous
smtpd_sasl_type = dovecot
smtpd_tls_auth_only = yes
smtpd_tls_cert_file = /etc/letsencrypt/live/libc.it/fullchain.pem
smtpd_tls_key_file = /etc/letsencrypt/live/libc.it/privkey.pem
smtpd_tls_ciphers = high
smtpd_tls_protocols = !SSLv2, !SSLv3
smtpd_tls_mandatory_protocols = !SSLv2, !SSLv3
smtpd_tls_mandatory_ciphers = high
smtpd_tls_dh1024_param_file = /etc/ssl/certs/dhparams.pem
smtpd_tls_security_level = may
smtpd_tls_session_cache_database = btree:${data_directory}/smptd_scache
smtpd_tls_loglevel = 1
smtpd_tls_received_header = yes
smtpd_use_tls = yes

# mail filters (DKIM, DMARC, rspamd)
smtpd_milters = unix:/run/opendkim/opendkim.sock,
	unix:/run/opendmarc/opendmarc.sock,
	inet:127.0.0.1:11332
non_smtpd_milters = $smtpd_milters
# rspamd mail filter macro
milter_mail_macros = i {mail_addr} {client_addr} {client_name} {auth_authen} {auth_type}

virtual_uid_maps = static:5000
virtual_gid_maps = static:5000
virtual_mailbox_base = /var/mail

append_dot_mydomain = no
biff = no
disable_vrfy_command = yes
readme_directory = no
recipient_delimiter = +-
mailbox_size_limit = 0
message_size_limit = 524288000
strict_mailbox_ownership = no
always_add_missing_headers = yes

# useless (since virtual_alias_maps exists), but disables dict_nis_init warning
alias_maps = hash:/etc/aliases

## rate limiting
# anvil limits
anvil_rate_time_unit = 60s
smtpd_client_connection_rate_limit = 10
smtpd_client_message_rate_limit = 2
smtpd_client_recipient_rate_limit = 10
# destination limits
default_destination_concurrency_limit = 1
default_destination_rate_delay = 60s
default_destination_recipient_limit = 2
