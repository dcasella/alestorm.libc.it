<?php

$prefs['_GLOBAL']['fixed'] = true;
$prefs['_GLOBAL']['hide_preferences'] = false;
$prefs['_GLOBAL']['pwstore_scheme'] = 'des_key';
$prefs['_GLOBAL']['suppress_version_warning'] = false;
$prefs['_GLOBAL']['sync_collection_workaround'] = false;
//$prefs['Nextcloud'] = array(
//	'name'              => '%N',
//	'username'          => '%l',
//	'password'          => '%p',
//	'url'               => 'https://cloud.libc.it/remote.php/dav/addressbooks/users/%l/contacts/',
//	'readonly'          => true,
//	'use_categories'    => true,
//	'fixed'             => array('username', 'use_categories'),
//);
