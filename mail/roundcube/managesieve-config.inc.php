<?php

$config['managesieve_host'] = 'tls://mx.libc.it';
$config['managesieve_auth_type'] = 'PLAIN';
$config['managesieve_default'] = '/var/mail/sieve/default.sieve';
$config['managesieve_vacation'] = 1;
$config['managesieve_forward'] = 1;
