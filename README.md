# Server Configuration

## Install

### Puppet

```sh
wget https://apt.puppetlabs.com/puppet-release-bullseye.deb
dpkg -i puppet-release-bullseye.deb
rm -f puppet-release-bullseye.deb
apt update
apt install -y puppet-agent
```

#### Puppet modules

```sh
for puppet_dep_module in puppetlabs-stdlib puppetlabs-inifile puppetlabs-pwshlib puppetlabs-powershell puppetlabs-reboot puppetlabs-concat herculesteam-augeasproviders_core; do /opt/puppetlabs/bin/puppet module install --ignore-dependencies "${puppet_dep_module}"; done
for puppet_module in puppet-systemd puppetlabs-apt herculesteam-augeasproviders_sysctl puppet-cron puppetlabs-docker puppetlabs-firewall puppet-gitlab_ci_runner puppet-logrotate puppet-openvpn markt-rspamd puppet-strongswan; do /opt/puppetlabs/bin/puppet module install --ignore-dependencies "${puppet_module}"; done
```

## DH Parameters

```sh
openssl dhparam -out /etc/ssl/certs/dhparams.pem 2048
```

## DKIM

```sh
for domain in libc.it {...}; do sudo -u opendkim mkdir -p "/etc/dkimkeys/${domain}"; sudo -u opendkim opendkim-genkey -D "/etc/dkimkeys/${domain}" -d "${domain}" -s default; done
```
