#!/usr/bin/env bash

set -o errexit  # exit script when a command fails
set -o pipefail # non-zero exit codes propagate to the end of a pipeline

usage() {
	tee <<-EOD
	USAGE:
	    ${0##*/} [FLAG...] [OPTION...] DIRECTORY

	FLAGS:
	    -h                Display this message

	    All flags exit the program
	    Flags are parsed in the displayed order

	OPTIONS:
	    -c                Create the backup directory if it does not exist

	    -m MAIL_ADDRESS   Send report email to MAIL_ADDRESS

	    -v                Verbose

	PARAMETERS:
	    DIRECTORY         Backup directory
	EOD
}

FILEDATE="$(date +'%Y%m%d')"

global_log="$(mktemp --suffix ".${0##*/}.log")"
verbose=''
directory=''

log() {
	local log_line
	log_line="[$(date +'%FT%R:%S%z')] ${0##*/}: ${*}"

	if [ -n "${verbose}" ]; then
		# stdout
		echo "${log_line}"
		# log file
		echo "${log_line}" >> /var/log/backup-data.log
	fi
	# global_log variable
	echo "${log_line}" >> "${global_log}"
}

cmd() {
	local cmd="${1}"

	# get cmd path
	command -v "${cmd}"
}

need_cmd() {
	local cmd="${1}"

	# exit if cmd doesn't exists
	cmd "${cmd}" > /dev/null 2>&1 || {
		echo "Command ${cmd} is needed to run this script"
		exit 127
	}
}

# shellcheck disable=SC2012
clear_backups() {
	log "(${FUNCNAME[0]}) Performing tmpreaper ..."

	if [ ! -d "${directory}" ]; then
		log "(${FUNCNAME[0]}) ... Canceled"
	fi

	for subdirectory in "${directory}"/*; do
		if [ "$(ls -1 "${subdirectory}" | wc -l)" -le 2 ]; then
			log "(${FUNCNAME[0]}) ... Canceled ${subdirectory}"
		else
			$(cmd tmpreaper) --showdeleted --mtime 7d \
				"$(readlink -f "${subdirectory}")"

			log "(${FUNCNAME[0]}) ... Done ${subdirectory}"
		fi
	done

	log "(${FUNCNAME[0]}) ... Done"
}

backup_docker_volume() {
	local volume="${1}"
	shift
	local backup_paths=("${@}")

	log "(${FUNCNAME[0]} - ${volume}) Performing ${volume} tar gzip ..."

	mkdir -p "${directory}/docker-${volume}"

	docker run --rm \
		--volumes-from "${volume}" \
		-v "${directory}/docker-${volume}/:/backup" \
		alpine \
		tar -c --gzip \
			--file="/backup/${FILEDATE}.tar.gz" \
			"${backup_paths[@]}"

	log "(${FUNCNAME[0]} - ${volume}) ... Done"
}

backup_docker_mysql() {
	local container="${1}"

	if ! docker container inspect "${container}" > /dev/null 2>&1; then
		log "(${FUNCNAME[0]} - ${container}) No such container: ${container}"
		return
	fi

	log "(${FUNCNAME[0]} - ${container}) Performing ${container} mysqldump gzip ..."

	mkdir -p "${directory}/docker-${container}"

	docker exec \
		"${container}" \
		sh -c 'exec mariadb-dump --default-character-set=utf8mb4 --all-databases' |
			gzip > "${directory}/docker-${container}/${FILEDATE}.sql.gz"

	log "(${FUNCNAME[0]} - ${container}) ... Done"
}

backup_mail() {
	log "(${FUNCNAME[0]}) Performing tar gzip ..."

	mkdir -p "${directory}/var-mail"

	tar -c --gzip \
		--file="${directory}/var-mail/${FILEDATE}.tar.gz" \
		/var/mail

	log "(${FUNCNAME[0]}) ... Done"
}

main() {
	local createdir mailto

	while getopts ':hcm:v' opt; do
		case "${opt}" in
		h)
			usage
			return
			;;
		c)
			createdir='true'
			;;
		m)
			mailto="${OPTARG}"
			;;
		v)
			verbose='true'
			;;
		\?)
			echo "Option -${OPTARG} is invalid"
			return 1
			;;
		:)
			echo "Option -${OPTARG} requires an argument"
			return 1
			;;
		esac
	done
	shift $((OPTIND-1))

	if [ ${#@} == 0 ]; then
		usage

		return 1
	fi

	need_cmd tmpreaper
	need_cmd docker

	directory="${1}"

	if [ -n "${createdir}" ]; then
		mkdir -p "${directory}"
	fi

	if [ ! -d "${directory}" ]; then
		echo "${0##*/}: cannot access '${directory}': No such file or directory"

		return 2
	fi

	{
		clear_backups

		# run backup jobs in background
		backup_docker_mysql mail-mysql &
		backup_docker_mysql nextcloud-mysql &
		backup_docker_volume roundcube /var/roundcube/config /var/roundcube/db &
		backup_docker_volume rspamd-redis /data &
		backup_docker_volume nextcloud-redis /data &
		backup_docker_volume teamspeak-server /var/ts3server &
		backup_docker_volume nextcloud /var/www/html &
		backup_mail &

		# wait for jobs to finish
		wait

		log 'Backup successful'

		if [ -n "${mailto}" ]; then
			mail "${mailto}" -s "Backup OK for $(hostname)" -r 'alestorm@libc.it' -a 'From: backup-data <alestorm@libc.it>' < "${global_log}"
		fi
		rm -f "${global_log}"
	} || {
		log 'Backup error'

		if [ -n "${mailto}" ]; then
			mail "${mailto}" -s "Backup ERR for $(hostname)" -r 'alestorm@libc.it' -a 'From: backup-data <alestorm@libc.it>' < "${global_log}"
		fi
		rm -f "${global_log}"

		return 3
	}
}

main "${@}"
