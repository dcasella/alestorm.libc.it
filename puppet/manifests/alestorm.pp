node 'default' {
  # Globals

  $domain_name = 'libc.it'

  # Packages

  Package { ensure => 'installed' }

  package { [
    'apparmor-utils', # fuck Docker
    'bind9-host',
    'bind9-utils',
    'bind9-dnsutils',
    'dovecot-imapd',
    'dovecot-lmtpd',
    'dovecot-managesieved',
    'dovecot-mysql',
    'dovecot-sieve',
    'iptables-persistent',
    'jq',
    'lsof',
    'man-db',
    'netcat-openbsd',
    'opendkim',
    'opendkim-tools',
    'opendmarc',
    'postfix',
    'postfix-mysql',
    'unattended-upgrades',
    'xfsprogs',
  ]: }

  # Filesystem

  file { '/var/lib/docker':
    ensure => directory,
  }

  file { '/var/local/backups':
    ensure => directory,
  }

  # Sysctl

  sysctl { 'net.core.somaxconn':
    value => 4096,
  }

  sysctl { 'net.ipv4.ip_forward':
    value => 1,
  }

  sysctl { 'net.ipv6.conf.all.forwarding':
    value => 1,
  }

  # Users

  group { 'vmail':
    ensure => present,
    gid    => 5000,
  }

  user { 'vmail':
    ensure     => present,
    uid        => 5000,
    gid        => 5000,
    home       => '/var/mail',
    managehome => true,
    shell      => '/usr/sbin/nologin',
  }

  user { 'postfix':
    ensure => present,
    groups => [
      'opendkim',
      'opendmarc',
    ],
  }

  # Cron

  cron { 'nextcloud-cron':
    command => '/usr/bin/docker exec --user www-data nextcloud php -f /var/www/html/cron.php',
  }

  # cron { 'puppet-apply':
  #   command => 'for manifest in /etc/puppetlabs/code/environments/production/manifests/*.pp; do /opt/puppetlabs/bin/puppet apply "${manifest}" >> /var/log/puppetlabs/puppet/puppet.log 2>&1; done', # lint:ignore:140chars lint:ignore:single_quote_string_with_variables
  #   minute  => 30,
  # }

  cron { 'backup-data':
    command     => "/usr/local/bin/backup-data -c -m backup@${domain_name} -v /var/local/backups > /dev/null 2>&1",
    environment => 'PATH=/usr/sbin:/usr/bin',
    hour        => 3,
    minute      => 0,
  }

  cron { 'update-containers':
    command     => "/usr/local/bin/update-containers -m monitor@${domain_name} -rp crazymax/diun:latest monogramm/autodiscover-email-settings:latest postfixadmin:latest roundcube/roundcubemail:latest teamspeak:latest > /dev/null 2>&1", # lint:ignore:140chars
    environment => 'PATH=/usr/sbin:/usr/bin',
    hour        => 4,
    minute      => 0,
  }

  cron { 'docker-cleanup':
    command => '/usr/bin/docker system prune --force --volumes > /var/log/docker-cleanup.log 2>&1',
    hour    => 5,
    minute  => 0,
  }

  # Logrotate

  logrotate::rule { 'backup-data':
    path          => '/var/log/backup-data.log',
    compress      => true,
    delaycompress => true,
    missingok     => true,
    rotate        => 4,
    rotate_every  => 'week',
  }

  logrotate::rule { 'update-containers':
    path          => '/var/log/update-containers.log',
    compress      => true,
    delaycompress => true,
    missingok     => true,
    rotate        => 4,
    rotate_every  => 'week',
  }

  # Rspamd

  include rspamd

  # Docker

  include docker

  ## Networks

  docker_network { 'nextcloud-net':
    ensure => present,
  }

  docker_network { 'mail-net':
    ensure => present,
  }

  ## Diun

  docker::run { 'diun':
    ensure                    => present,
    image                     => 'crazymax/diun:latest',
    hostname                  => 'diun',
    volumes                   => [
      'diun:/data',
      '/var/run/docker.sock:/var/run/docker.sock',
      '/root/docker:/data/custom-images:ro',
    ],
    env                       => [
      'TZ=Europe/Rome',
      'LOG_LEVEL=info',
      'LOG_JSON=false',
      'DIUN_WATCH_SCHEDULE=0 */6 * * *',
      'DIUN_PROVIDERS_DOCKER=true',
      'DIUN_PROVIDERS_DOCKERFILE_PATTERNS=/data/custom-images/*/Dockerfile',
      "DIUN_NOTIF_MAIL_HOST=mx.${domain_name}",
      'DIUN_NOTIF_MAIL_PORT=587',
      'DIUN_NOTIF_MAIL_USERNAME=monitor@libc.it',
      'DIUN_NOTIF_MAIL_PASSWORD=TEMPLATE_MONITOR_MAIL_PASSWORD',
      'DIUN_NOTIF_MAIL_FROM=alestorm@libc.it',
      'DIUN_NOTIF_MAIL_TO=monitor@libc.it',
    ],
    remove_container_on_start => false,
  }

  ## Redis

  docker::run { 'rspamd-redis':
    ensure                    => present,
    image                     => 'redis:alpine',
    labels                    => [
      'diun.enable=true',
      'diun.notify_on=update',
    ],
    ports                     => [
      '127.0.0.1:6380:6379',
    ],
    volumes                   => [
      'rspamd-redis:/data',
    ],
    remove_container_on_start => false,
  }

  docker::run { 'nextcloud-redis':
    ensure                    => present,
    image                     => 'redis:alpine',
    labels                    => [
      # duplicate
      # 'diun.enable=true',
      # 'diun.notify_on=update',
    ],
    net                       => [
      'nextcloud-net',
    ],
    volumes                   => [
      'nextcloud-redis:/data',
    ],
    remove_container_on_start => false,
  }

  ## MySQL / MariaDB

  docker::run { 'mail-mysql':
    ensure                    => present,
    image                     => 'mariadb:lts',
    labels                    => [
      'diun.enable=true',
      'diun.notify_on=update',
    ],
    net                       => [
      'mail-net',
    ],
    ports                     => [
      '127.0.0.1:3306:3306',
    ],
    volumes                   => [
      'mail-mysql:/var/lib/mysql',
      'mysql-conf-latin1:/etc/mysql/conf.d',
      '/var/lib/docker/volumes/mysql-conf-latin1/_data/.my.cnf:/root/.my.cnf:ro',
    ],
    env                       => [
      'MARIADB_ROOT_PASSWORD=TEMPLATE_ROOT_PASSWORD',
      'MARIADB_DATABASE=mail',
      'MARIADB_USER=mail',
      'MARIADB_PASSWORD=TEMPLATE_MAIL_PASSWORD',
      'MARIADB_AUTO_UPGRADE=1',
    ],
    remove_container_on_start => false,
  }

  docker::run { 'nextcloud-mysql':
    ensure                    => present,
    image                     => 'mariadb:lts',
    labels                    => [
      # duplicate
      # 'diun.enable=true',
      # 'diun.notify_on=update',
    ],
    net                       => [
      'nextcloud-net',
    ],
    volumes                   => [
      'nextcloud-mysql:/var/lib/mysql',
      'mysql-conf-utf8:/etc/mysql/conf.d',
      '/var/lib/docker/volumes/mysql-conf-utf8/_data/.my.cnf:/root/.my.cnf:ro',
    ],
    env                       => [
      'MARIADB_ROOT_PASSWORD=TEMPLATE_ROOT_PASSWORD',
      'MARIADB_DATABASE=nextcloud',
      'MARIADB_USER=nextcloud',
      'MARIADB_PASSWORD=TEMPLATE_NEXTCLOUD_PASSWORD',
      'MARIADB_AUTO_UPGRADE=1',
    ],
    remove_container_on_start => false,
  }

  ## Nextcloud

  docker::run { 'nextcloud':
    image                     => 'nextcloud:stable',
    labels                    => [
      'diun.enable=true',
      'diun.notify_on=update',
    ],
    net                       => [
      'nextcloud-net',
    ],
    ports                     => [
      '127.0.0.1:8123:80',
    ],
    volumes                   => [
      'nextcloud:/var/www/html',
    ],
    remove_container_on_start => false,
  }

  ## Teamspeak

  docker::run { 'teamspeak-server':
    image                     => 'teamspeak:latest',
    ports                     => [
      '9987:9987/udp',
      '30033:30033',
      '127.0.0.1:10011:10011', # console
    ],
    volumes                   => ['teamspeak-server:/var/ts3server'],
    env                       => [
      'TS3SERVER_LICENSE=accept',
      'TS3SERVER_LOG_APPEND=1',
    ],
    remove_container_on_start => false,
  }

  ## Roundcube

  docker::run { 'roundcube':
    image                     => 'roundcube/roundcubemail:latest',
    ports                     => [
      '127.0.0.1:10080:80'
    ],
    volumes                   => [
      'roundcube:/var/roundcube/config',
      'roundcube-db:/var/roundcube/db',
    ],
    env                       => [
      "ROUNDCUBEMAIL_DEFAULT_HOST=ssl://mx.${domain_name}",
      'ROUNDCUBEMAIL_DEFAULT_PORT=993',
      "ROUNDCUBEMAIL_SMTP_SERVER=tls://mx.${domain_name}",
      'ROUNDCUBEMAIL_SMTP_PORT=587',
      'ROUNDCUBEMAIL_COMPOSER_PLUGINS=johndoh/contextmenu:3.3.1,roundcube/carddav:v5.1.0,melanie2/infinitescroll:dev-master',
      'ROUNDCUBEMAIL_PLUGINS=archive,managesieve,markasjunk,newmail_notifier,vcard_attachments,zipdownload,contextmenu,carddav,infinitescroll', # lint:ignore:140chars
      'ROUNDCUBEMAIL_UPLOAD_MAX_FILESIZE=512M',
      'ROUNDCUBEMAIL_ASPELL_DICTS=en,it,fr',
      'ROUNDCUBEMAIL_DB_TYPE=sqlite',
    ],
    remove_container_on_start => false,
  }

  ## Postfixadmin

  docker::run { 'postfixadmin':
    ensure                    => present,
    image                     => 'postfixadmin:latest',
    net                       => [
      'mail-net',
    ],
    ports                     => [
      '127.0.0.1:8085:80',
    ],
    volumes                   => [
      'postfixadmin:/mnt/dummy',
      '/var/lib/docker/volumes/postfixadmin/_data/config.local.php:/var/www/html/config.local.php:ro',
    ],
    env                       => [
      'POSTFIXADMIN_SETUP_PASSWORD=TEMPLATE_MAILSETUP_PASSWORD',
      'POSTFIXADMIN_DB_TYPE=mysqli',
      'POSTFIXADMIN_DB_HOST=mail-mysql', # container on network
      'POSTFIXADMIN_DB_USER=mail',
      'POSTFIXADMIN_DB_NAME=mail',
      'POSTFIXADMIN_DB_PASSWORD=TEMPLATE_MAIL_PASSWORD',
    ],
    remove_container_on_start => false,
  }

  ## Mail Autodiscover / Autoconfig

  docker::run { 'mail-autodiscover':
    ensure                    => present,
    image                     => 'monogramm/autodiscover-email-settings:latest',
    ports                     => [
      '127.0.0.1:8000:8000',
    ],
    env                       => [
      "COMPANY_NAME=${domain_name}",
      "SUPPORT_URL=https://autodiscover.${domain_name}",
      "DOMAIN=${domain_name}",
      "IMAP_HOST=mx.${domain_name}",
      'IMAP_PORT=993',
      'IMAP_SOCKET=SSL',
      "SMTP_HOST=mx.${domain_name}",
      'SMTP_PORT=587',
      'SMTP_SOCKET=STARTTLS',
    ],
    remove_container_on_start => false,
  }

  # GitLab CI Runner

  class { 'gitlab_ci_runner':
    manage_docker   => false,
    concurrent      => 7,
    runner_defaults => {
      'url'                => 'https://gitlab.com/ci',
      'registration-token' => 'TEMPLATE_RUNNER_TOKEN',
      'locked'             => false,
      'executor'           => 'docker',
      'docker'             => {
        'image'       => 'ci-gitlab-runner:latest',
        'pull_policy' => 'never',
      },
    },
    runners         => {
      'gitlab-runner-01' => { 'name' => 'gitlab-runner-01' },
      'gitlab-runner-02' => { 'name' => 'gitlab-runner-02' },
      'gitlab-runner-03' => { 'name' => 'gitlab-runner-03' },
      'gitlab-runner-04' => { 'name' => 'gitlab-runner-04' },
      'gitlab-runner-05' => { 'name' => 'gitlab-runner-05' },
      'gitlab-runner-06' => { 'name' => 'gitlab-runner-06' },
      'gitlab-runner-07' => { 'name' => 'gitlab-runner-07' },
    },
  }

  # VPNs

  ## OpenVPN

  class { 'openvpn':
    default_easyrsa_ver => '3.0',
  }

  openvpn::server { "vpn.${domain_name}":
    country        => 'IT',
    province       => 'MI',
    city           => 'Milan',
    organization   => 'IT',
    email          => "monitor@${domain_name}",
    common_name    => "vpn.${domain_name}",
    logfile        => '/var/log/openvpn/openvpn.log',
    proto          => udp,
    route          => ['10.11.94.0 255.255.255.0'],
    server         => '10.11.94.0 255.255.255.0',
    topology       => subnet,
    tls_server     => true,
    crl_auto_renew => true,
    autostart      => true,
  }

  openvpn::client { "dcasella@${domain_name}":
    server          => "vpn.${domain_name}",
    proto           => udp,
    remote_host     => "vpn.${domain_name}",
    x509_name       => "vpn.${domain_name}",
    pull            => true,
    remote_cert_tls => true,
  }

  openvpn::client_specific_config { "dcasella@${domain_name}":
    server           => "vpn.${domain_name}",
    redirect_gateway => true,
  }

  openvpn::client { "mluisi@${domain_name}":
    server          => "vpn.${domain_name}",
    proto           => udp,
    remote_host     => "vpn.${domain_name}",
    x509_name       => "vpn.${domain_name}",
    pull            => true,
    remote_cert_tls => true,
  }

  openvpn::client_specific_config { "mluisi@${domain_name}":
    server           => "vpn.${domain_name}",
    redirect_gateway => true,
  }

  openvpn::client { "aleo@${domain_name}":
    server          => "vpn.${domain_name}",
    proto           => udp,
    remote_host     => "vpn.${domain_name}",
    x509_name       => "vpn.${domain_name}",
    pull            => true,
    remote_cert_tls => true,
  }

  openvpn::client_specific_config { "aleo@${domain_name}":
    server           => "vpn.${domain_name}",
    redirect_gateway => true,
  }

  openvpn::client { "aciulpan@${domain_name}":
    server          => "vpn.${domain_name}",
    proto           => udp,
    remote_host     => "vpn.${domain_name}",
    x509_name       => "vpn.${domain_name}",
    pull            => true,
    remote_cert_tls => true,
  }

  openvpn::client_specific_config { "aciulpan@${domain_name}":
    server           => "vpn.${domain_name}",
    redirect_gateway => true,
  }

  ## IPsec

  include strongswan

  class { 'strongswan::setup':
    options => {},
  }

  strongswan::secrets { '%any':
    options => {
      'PSK' => 'TEMPLATE_IPSEC_PSK',
    },
  }

  strongswan::conn { '%default':
    options => {
      keylife     => '20m',
      rekeymargin => '3m',
      keyingtries => '1',
      keyexchange => 'ike',
      dpddelay    => '10s',
    },
  }

  strongswan::conn { 'home-p1':
    options => {
      type         => 'tunnel',
      auto         => 'add',
      lifetime     => '28800s',
      ike          => 'aes256-sha256-modp2048!',
      left         => $::ipaddress,
      leftauth     => 'psk',
      leftfirewall => 'yes',
      right        => '%any',
      rightauth    => 'psk',
    },
  }

  strongswan::conn { 'home-p2-0':
    options => {
      auto        => 'add',
      esp         => 'aes128gcm16-sha256-modp1536!',
      leftsubnet  => '10.19.0.0/16',
      rightsubnet => '192.168.1.0/24',
      also        => 'home-p1',
    },
  }

  strongswan::conn { 'home-p2-1':
    options => {
      auto        => 'add',
      esp         => 'aes128gcm16-sha256-modp1536!',
      leftsubnet  => '10.19.0.0/16',
      rightsubnet => '192.168.2.0/24',
      also        => 'home-p1',
    },
  }

  strongswan::charon { '/var/log/ipsec.log':
    options => {
      filelog => {
        charon => {
          path        => '/var/log/ipsec.log',
          time_format => '%b %e %T',
          ike_name    => 'yes',
          append      => 'no',
          default     => 1,
          flush_line  => 'yes',
        },
        stderr => {
          ike => 2,
          knl => 2,
        },
      },
    },
  }

  # Firewall

  ## Basic needs

  firewall { '000 accept input for all to loopback interface':
    chain   => 'INPUT',
    jump    => 'accept',
    iniface => 'lo',
    proto   => all,
  }

  firewall { '000 accept input for all to loopback interface (v6)':
    chain    => 'INPUT',
    jump     => 'accept',
    iniface  => 'lo',
    proto    => all,
    protocol => 'ip6tables',
  }

  firewall { '001 accept all for all related/established connections':
    chain   => 'INPUT',
    jump    => 'accept',
    ctstate => ['RELATED', 'ESTABLISHED'],
    proto   => all,
  }

  firewall { '001 accept all for all related/established connections (v6)':
    chain    => 'INPUT',
    jump     => 'accept',
    ctstate  => ['RELATED', 'ESTABLISHED'],
    proto    => all,
    protocol => 'ip6tables',
  }

  firewall { '002 accept all for icmp':
    chain  => 'INPUT',
    jump   => 'accept',
    proto  => icmp,
  }

  firewall { '002 accept all for icmp (v6)':
    chain    => 'INPUT',
    jump     => 'accept',
    proto    => ipv6-icmp,
    protocol => 'ip6tables',
  }

  ## Core services

  firewall { '003 accept input for tcp to ssh':
    chain  => 'INPUT',
    jump   => 'accept',
    dport  => 22,
    proto  => tcp,
  }

  firewall { '003 accept input for tcp to ssh (v6)':
    chain    => 'INPUT',
    jump     => 'accept',
    dport    => 22,
    proto    => tcp,
    protocol => 'ip6tables',
  }

  firewall { '004 accept input for tcp to mail':
    chain  => 'INPUT',
    jump   => 'accept',
    dport  => [
      25,   # SMTP
      587,  # MSA
      993,  # IMAPS
      4190, # Managesieve
    ],
    proto  => tcp,
  }

  firewall { '004 accept input for tcp to mail (v6)':
    chain    => 'INPUT',
    jump     => 'accept',
    dport    => [
      25,   # SMTP
      587,  # MSA
      993,  # IMAPS
      4190, # Managesieve
    ],
    proto    => tcp,
    protocol => 'ip6tables',
  }

  firewall { '005 accept input for tcp to http https':
    chain  => 'INPUT',
    jump   => 'accept',
    dport  => [80, 443],
    proto  => tcp,
  }

  firewall { '005 accept input for tcp to http https (v6)':
    chain    => 'INPUT',
    jump     => 'accept',
    dport    => [80, 443],
    proto    => tcp,
    protocol => 'ip6tables',
  }

  ## VPNs

  firewall { '100 accept input for udp to ipsec-isakmp':
    chain  => 'INPUT',
    jump   => 'accept',
    dport  => 500,
    proto  => udp,
  }

  firewall { '100 accept input for udp to ipsec-isakmp (v6)':
    chain    => 'INPUT',
    jump     => 'accept',
    dport    => 500,
    proto    => udp,
    protocol => 'ip6tables',
  }

  firewall { '101 accept input for udp to ipsec-nat-t':
    chain  => 'INPUT',
    jump   => 'accept',
    dport  => 4500,
    proto  => udp,
  }

  firewall { '101 accept input for udp to ipsec-nat-t (v6)':
    chain    => 'INPUT',
    jump     => 'accept',
    dport    => 4500,
    proto    => udp,
    protocol => 'ip6tables',
  }

  firewall { '102 accept input for esp':
    chain  => 'INPUT',
    jump   => 'accept',
    proto  => esp,
  }

  firewall { '102 accept input for esp (v6)':
    chain    => 'INPUT',
    jump     => 'accept',
    proto    => esp,
    protocol => 'ip6tables',
  }

  firewall { '110 accept input for all from home LAN to internal network':
    chain       => 'INPUT',
    jump        => 'accept',
    source      => '192.168.1.0/24',
    destination => '10.19.0.0/16',
    proto       => all,
  }

  firewall { '110 accept input for all from home WiFi to internal network':
    chain       => 'INPUT',
    jump        => 'accept',
    source      => '192.168.2.0/24',
    destination => '10.19.0.0/16',
    proto       => all,
  }

  firewall { '200 accept input for udp to openvpn':
    chain  => 'INPUT',
    jump   => 'accept',
    dport  => 1194,
    proto  => udp,
  }

  firewall { '200 accept input for udp to openvpn (v6)':
    chain    => 'INPUT',
    jump     => 'accept',
    dport    => 1194,
    proto    => udp,
    protocol => 'ip6tables',
  }

  ## Docker containers

  firewall { '400 accept input for tcp to teamspeak':
    chain  => 'INPUT',
    jump   => 'accept',
    dport  => 30033,
    proto  => tcp,
  }

  firewall { '400 accept input for tcp to teamspeak (v6)':
    chain    => 'INPUT',
    jump     => 'accept',
    dport    => 30033,
    proto    => tcp,
    protocol => 'ip6tables',
  }

  firewall { '401 accept input for udp to teamspeak':
    chain  => 'INPUT',
    jump   => 'accept',
    dport  => 9987,
    proto  => udp,
  }

  firewall { '401 accept input for udp to teamspeak (v6)':
    chain    => 'INPUT',
    jump     => 'accept',
    dport    => 9987,
    proto    => udp,
    protocol => 'ip6tables',
  }

  firewall { '999 drop input for all':
    chain  => 'INPUT',
    jump   => 'drop',
    before => undef,
    proto  => all,
  }

  firewall { '999 drop input for all (v6)':
    chain    => 'INPUT',
    jump     => 'drop',
    before   => undef,
    proto    => all,
    protocol => 'ip6tables',
  }

  ## Forwarding

  firewall { '000 accept forward for all related/established connections':
    chain   => 'FORWARD',
    jump    => 'accept',
    ctstate => ['RELATED', 'ESTABLISHED'],
    proto   => all,
  }

  ### OpenVPN

  firewall { '010 accept forward for all new connections from vpn':
    chain    => 'FORWARD',
    jump     => 'accept',
    ctstate  => 'NEW',
    iniface  => tun0,
    outiface => eth0,
    proto    => all,
    source   => '10.11.94.0/24',
  }

  firewall { '011 nat to masquerade postrouting for all from vpn':
    chain    => 'POSTROUTING',
    jump     => 'MASQUERADE',
    proto    => all,
    outiface => eth0,
    source   => '10.11.94.0/24',
    table    => nat,
  }

  firewall { '999 drop forward for all':
    chain  => 'FORWARD',
    jump   => 'drop',
    before => undef,
    proto  => all,
  }
}
